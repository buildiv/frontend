import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

import Nprogress from "nprogress";
import "nprogress/nprogress.css";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      component: () => import("./views/About.vue")
    },
    {
      path: "/register",
      name: "register",
      component: () => import("./views/Register.vue"),
      meta: {
        guest: true
      }
    },
    {
      path: "/build",
      name: "build",
      component: () => import("./views/Build.vue"),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/explore",
      name: "explore",
      component: () => import("./views/Explore.vue"),
      meta: {
        requiresAuth: false
      }
    },
    {
      path: "/constructor/:template",
      name: "constructorTemplate",
      component: () => import("./views/Constructor.vue"),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/constructor",
      name: "constructorNew",
      component: () => import("./views/Constructor.vue"),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "*",
      component: () => import("./views/PageNotFound.vue")
    }
  ]
});

router.beforeResolve((to, from, next) => {
  if (to.name) {
    Nprogress.start();
  }

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (localStorage.getItem("token") == null) {
      next({
        path: "/register",
        params: { nextUrl: to.fullPath }
      });
    } else {
      let user = JSON.parse(localStorage.getItem("user"));
      if (to.matched.some(record => record.meta.admin)) {
        if (user && user.role === "admin") {
          next();
        } else {
          next({ name: "home" });
        }
      } else {
        next();
      }
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (localStorage.getItem("token") == null) {
      next();
    } else {
      next({ name: "home" });
    }
  } else {
    next();
  }
});

router.afterEach(() => {
  Nprogress.done();
});

export default router;
