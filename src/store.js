import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import AuthService from "@/services/AuthService";

Vue.prototype.$http = axios;
const token = localStorage.getItem("token");
if (token) {
  Vue.prototype.$http.defaults.headers.common["Authorization"] = token;
}

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    status: "",
    token: localStorage.getItem("token") || "",
    user: JSON.parse(localStorage.getItem("user")) || {},
    loginModal: false,
    template: [],
    templateId: ""
  },
  mutations: {
    auth_request(state) {
      state.status = "loading";
    },
    auth_success(state, payload) {
      state.status = "success";
      ({ token: state.token, user: state.user } = payload);
    },
    auth_error(state) {
      state.status = "error";
    },
    logout(state) {
      state.status = "";
      state.token = "";
    },
    show_moldal(state) {
      state.loginModal = true;
    },
    hide_modal(state) {
      state.loginModal = false;
    },
    setTemplateId(state, payload) {
      ({ id: state.templateId } = payload);
    },
    add_component(state, payload) {
      const { component, type } = payload;
      const last = state.template.length;
      if (type === "header") {
        if (state.template[0] && state.template[0].type == "header")
          state.template[0] = { component, type };
        else {
          state.template.unshift({ component, type });
        }
      }
      if (
        last &&
        state.template[last] &&
        state.template[last].type == "footer"
      ) {
        if (type === "footer") {
          state.template[last] = { component, type };
        } else {
          state.template[last - 1] = { component, type };
        }
      } else {
        state.template.push({ component, type });
      }
      state.template.push({ component, type });
    }
  },
  actions: {
    login({ commit }, data) {
      return new Promise((resolve, reject) => {
        commit("auth_request");
        AuthService.login(data)
          .then(res => {
            const user = res.data.user;
            localStorage.setItem("user", JSON.stringify(user));

            const token = res.data.token;
            localStorage.setItem("token", token);

            axios.defaults.headers.common["Authorization"] = token;
            commit("auth_success", { token, user });
            resolve(res);
          })
          .catch(err => {
            commit("auth_error");
            localStorage.removeItem("token");
            reject(err.response);
          });
      });
    },
    register({ commit }, data) {
      return new Promise((resolve, reject) => {
        commit("auth_request");
        AuthService.signUp(data)
          .then(res => {
            const user = res.data.user;
            localStorage.setItem("user", JSON.stringify(user));

            const token = res.data.token;
            localStorage.setItem("token", token);
            axios.defaults.headers.common["Authorization"] = token;
            commit("auth_success", { token, user });
            resolve(res);
          })
          .catch(err => {
            commit("auth_error");
            localStorage.removeItem("token");
            reject(err);
          });
      });
    },
    logout({ commit }) {
      return new Promise((resolve, reject) => { // eslint-disable-line
        commit("hide_modal");
        commit("logout");
        localStorage.removeItem("user");
        localStorage.removeItem("token");
        delete axios.defaults.headers.common["Authorization"];
        resolve();
      });
    }
  },
  getters: {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    loginModal: state => state.loginModal,
    email: state => state.user.email,
    isAdmin: state => state.user.role === "admin",
    template: state => state.template,
    templateId: state => state.templateId
  }
});
