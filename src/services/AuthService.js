import Api from "@/services/Api";

export default {
  signUp(data) {
    return Api().post("/users/signup", data);
  },
  login(data) {
    return Api().post("/users/login", data);
  }
};
