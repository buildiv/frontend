import Api from "@/services/Api";

export default {
  get() {
    return Api().get("/contact");
  },
  send(data) {
    return Api().post("/contact", data);
  },
  delete(data) {
    return Api().delete("/contact", { params: data });
  }
};
