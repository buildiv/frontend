import Api from "@/services/Api";

export default {
  get(data) {
    return Api().get(`/template/${data.href}`);
  },
  getAll() {
    return Api().get("/template");
  },
  getMyPages(data) {
    return Api().get("/template/my", data);
  },
  save(data) {
    return Api().post("/template", data);
  },
  rename(data) {
    return Api().post("/template/rename", data);
  },
  delete(data) {
    return Api().delete("/template", { params: data });
  }
};
