import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";
import "reset-css/reset.css";

import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);

import VueFroala from "vue-froala-wysiwyg";
import "froala-editor/js/froala_editor.pkgd.min";
import "froala-editor/css/froala_editor.pkgd.min.css";
import "font-awesome/css/font-awesome.css";
import "froala-editor/css/froala_style.min.css";
Vue.use(VueFroala);

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "froala-design-blocks/dist/css/froala_blocks.min.css";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
