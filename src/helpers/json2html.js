import { isString, isEmpty, isArray, cloneDeep } from "lodash";

function json2html(dNode, h) {
  if (isEmpty(dNode)) {
    return [];
  }

  if (isArray(dNode)) {
    return dNode.map(child => json2html(child, h));
  }

  let children = [];

  if (dNode.children && dNode.children.length > 0) {
    dNode.children.forEach(c => {
      if (isString(c)) {
        children.push(c);
      } else {
        children.push(json2html(c, h));
      }
    });
  }

  const properties = cloneDeep(dNode.properties);
  return h(
    dNode.tagName,
    properties,
    children.length > 0 ? children : dNode.textNode
  );
}

export default json2html;
