const webpack = require("webpack");

module.exports = {
  devServer: {
    host: "0.0.0.0",
    hot: true,
    disableHostCheck: true
  },
  css: {
    loaderOptions: {
      sass: {
        data: `
            @import "@/scss/_variables.scss";
            @import "@/scss/_mixins.scss";
          `
      }
    }
  },
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        $: "jquery",
        jQuery: "jquery",
        "window.jQuery": "jquery"
      })
    ]
  }
};
